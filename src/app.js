const express = require('express')
const app = express()
const path = require('path')
const {v4} = require('uuid')
const morgan = require('morgan')

app.use(express.json())

// creation de la base de données
let CONTACTS = [
  {id: v4(), name: 'cristiano ronaldo', value: 'Manchester', marked: false}
]

// GET
app.get('/api/contacts', (req, res) => {
    setTimeout(() => {
      res.status(200).json(CONTACTS)
    }, 1000)
  })
  
  // POST
  app.post('/api/contacts', (req, res) => {
    const contact = {...req.body, id: v4(), marked: false}
    CONTACTS.push(contact)
    res.status(201).json(contact)
  })
  
  // DELETE
  app.delete('/api/contacts/:id', (req, res) => {
    CONTACTS = CONTACTS.filter(c => c.id !== req.params.id)
    res.status(200).json({message: 'Le formumair a ete suprimer'})
  })
  
  // PUT
  app.put('/api/contacts/:id', (req, res) => {
    const idx = CONTACTS.findIndex(c => c.id === req.params.id)
    CONTACTS[idx] = req.body
    res.json(CONTACTS[idx])
  })
  





app.use(express.static(path.resolve(__dirname, '../../jsau-webapp')))

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../../jsau-webapp', 'index.html'))
})

app.use(morgan('dev'))
app.listen(3002, ()=> console.log('server strat...'))
